/*
* Ej4
* Juan Carlos Zepeda Abrego
*/
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

int main(){

	char texto[31];
	int n, contador = 3;

	//Ingreso de texto
	syscall(SYS_write, 1, "Ingrese una frase:\t", 30);
	n = read(0, texto, sizeof texto -1);
	texto[n-1] = '\0';

	//imprimiendo frase
	for(int i=0; i < contador; i++ ){
		syscall(SYS_write, 1, "\n", 1);
		syscall(SYS_write, 1, texto, strlen(texto));
		syscall(SYS_write, 1, "\n", 1);
	}

return 0;
}
