/*
* Ejercicio 1
* Juan Carlos Zepeda Abrego
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(){

	int fd, cadena;
	char buffer[256];
	ssize_t largoMensaje;
	char msj[] = "Sistemas Operativos 2020\n";
	largoMensaje = strlen(msj);
	char ruta[] = "Mensaje en Pantalla";

	fd=open(ruta, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	cadena = write(fd, msj, largoMensaje);
	close(fd);

	fd=open(ruta, O_RDONLY);
	cadena = read(fd, buffer, cadena);
	cadena = write(1, buffer, cadena);
	close(fd);

return 0;
}
