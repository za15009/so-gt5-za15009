/*
* Ej2
* Juan Carlos Zepeda Abrego
*/

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

int main(){

	int ancho_buff = 256;
	int cadena, texto, origen, destino;
	char buffer[ancho_buff];

	//abriendo archivo
	origen = open("origen.txt", O_RDONLY);
	if(origen != -1){
		destino = creat("salida.txt", 0666);
		if(destino != 0){
			cadena = read(origen, buffer, ancho_buff);
			texto = write(destino, buffer, cadena);
		}
	close(origen);
	close(destino);
	}

return 0;
}
