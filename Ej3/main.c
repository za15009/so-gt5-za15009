/*
* Ej3
* Juan Carlos Zepeda Abrego
*/
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>

int main(){

	int ancho_buffer = 256;
	int origen, texto;
	char buffer[ancho_buffer];

	//abriendo archivo con contenido
	origen = open("origen.txt", O_RDONLY);
	if(origen < 0){
		syscall(SYS_write, 1, "Archivo no encontrado", 20);
	}else {
		texto = read(origen, buffer, ancho_buffer);
		syscall(SYS_write, 1, buffer, strlen(buffer));
	}
	close(origen);

return 0;
}
